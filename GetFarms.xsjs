/* --- Global variables --- */
var CONST_ROOT_PACKAGE = "TEST_package_EMAESTRE"; // IR7 Package in Content section
var CONST_NEO_SCHEMA = "ELEAZARMAESTRE"; // IR7 User Schema

/* --- Functions --- */
// Function for process the client request
function processRequest() {
	try {
		var api_call = {
				"farms": []
		};
		var conn = $.db.getConnection(); // Returns a connection to the database
		var query = "SELECT FARM_ID, FARM_NAME FROM " + "\"" + CONST_NEO_SCHEMA + "\"" + ".\"OS_FARM\"";
		var stmt = conn.prepareStatement(query); // Prepare statement with the required query
		var rs = stmt.executeQuery(); // Execute the statement
		
		while(rs.next()) {
			api_call.farms.push({
				farmID: rs.getInteger(1),
				farmName: rs.getString(2),
			});
		}
		
		stmt.close(); // Close the statement
		conn.commit(); // Commits the changes
		conn.close(); // Close connection
			
		$.response.contentType = "application/json"; // Content type of the response
		$.response.setBody(JSON.stringify(api_call)); // Set body with the selected string
		$.response.status = $.net.http.OK; // status = OK when all the task are done
	}
	catch(e) {
		$.response.contentType = "text/plain"; // Content type of the response
		$.response.setBody(e.name + ": " +  e.message); // Set body with the selected string
	}
}

/* --- Method Call --- */
processRequest();