/* --- Global variables --- */
var CONST_ROOT_PACKAGE = "TEST_package_EMAESTRE"; // IR7 Package in Content section
var CONST_NEO_SCHEMA = "ELEAZARMAESTRE"; // IR7 User Schema
var MORTALITY_PCT = [0, 0.005];

/* --- Functions --- */
function storeData(lot_id, shed_id, farm_id, today, actual_bird_balance, food_csmpt, unit_csmpt) {
	var conn = $.db.getConnection(); // Returns a connection to the database
	var query = "CALL " + "\"" + CONST_NEO_SCHEMA + "\"" + "." +  "\"" + CONST_ROOT_PACKAGE + ".ComfortFarms.Stored_Procedures::ProductionControl\"(?,?,?,?,?,?,?);";
	var stmt = conn.prepareStatement(query); // Prepare the SQL statement

	// Set the statement with the required parameters	
	stmt.setString(1, lot_id);
	stmt.setInteger(2, shed_id);
	stmt.setInteger(3, farm_id);
	stmt.setDate(4, today); 
	stmt.setInteger(5, actual_bird_balance);
	stmt.setFloat(6, food_csmpt);
	stmt.setString(7, unit_csmpt);

	stmt.execute(); // Execute the statement
	stmt.close(); // Close the statement
	conn.commit(); // Commits the changes
	conn.close(); // Close connection
}

function foodAverageCsmpt(bird_age) {
	var foodObject = {
		foodCsmpt: "",
		unit: ""
	};
	
	foodObject.unit = 'gr';
	if(bird_age >= 0 && bird_age <= 7) {
		foodObject.foodCsmpt = 0.13;
	}
	else if(bird_age >= 8 && bird_age <= 23) {
		foodObject.foodCsmpt = 0.87;
	}
	else if(bird_age >= 24 && bird_age <= 37) {
		foodObject.foodCsmpt = 1610.00;
	}
	else {
		foodObject.foodCsmpt = 2000.00;
	}
	 
	return foodObject;
}

function checkProductionControl(lot_id, shed_id, farm_id, initial_balance) {
	var actual_bird_balance;
	var min = 0;
	var array_pos = (Math.floor(Math.random() * ((MORTALITY_PCT.length - 1) - min + 1)) + min); // Random number for choose a mortality pct 
	var mortality_pct = MORTALITY_PCT[array_pos];
	var conn = $.db.getConnection(); // Returns a connection to the database
	var checkPC = "CALL " + "\"" + CONST_NEO_SCHEMA + "\"" + "." +  "\"" + CONST_ROOT_PACKAGE + ".ComfortFarms.Stored_Procedures::CheckProductionControl\"(?,?,?)";
	var stmtPC = conn.prepareCall(checkPC);
	
	stmtPC.setString(1, lot_id);
	stmtPC.setInteger(2, shed_id);
	stmtPC.setInteger(3, farm_id);
	stmtPC.execute();
	
	var rsPC = stmtPC.getResultSet();
	
	if(rsPC.next()) {
		actual_bird_balance = rsPC.getInteger(1) - Math.floor(rsPC.getInteger(1) * mortality_pct);
	}
	else {
		actual_bird_balance = initial_balance - Math.floor(initial_balance * mortality_pct);
	}
	
	stmtPC.close();
	conn.commit();
	conn.close();
	
	return actual_bird_balance;
}

function productionControlSimulator() {
	var lot_id,
		farm_id, 
		shed_id,
		initial_balance,
		actual_bird_balance, 
		food_csmpt,
		unit_csmpt,
		housing_date,
		bird_age,
		csmptRes;
	
	var local = new Date();
	var offset = -240;
	var today = new Date(local.getTime() + (offset*60*1000));
	var oneDay = 24*60*60*1000;
	
	var conn = $.db.getConnection(); // Returns a connection to the database
	var query = "SELECT LOT_ID, SHED_ID, FARM_ID, INITIAL_BIRD_BALANCE, HOUSING_DATE " + 
				"FROM \"" + CONST_NEO_SCHEMA + "\"" + ".\"TX_LOT\"";
	var stmt = conn.prepareStatement(query); // Prepare statement with the required query
	var rs = stmt.executeQuery(); // Execute the statement
	
	while(rs.next()) {
		// Get the results of the query
		lot_id = rs.getString(1);
		shed_id = rs.getInteger(2);
		farm_id = rs.getInteger(3);
		initial_balance = rs.getInteger(4);
		housing_date = rs.getDate(5);
		
		// Generate the data to insert
		bird_age =  Math.floor(Math.abs(housing_date.getTime() - today.getTime())/oneDay);
		csmptRes = foodAverageCsmpt(bird_age);
		food_csmpt = csmptRes.foodCsmpt;
		food_csmpt = parseFloat(food_csmpt.toFixed(2));
		unit_csmpt = csmptRes.unit;		
		actual_bird_balance = checkProductionControl(lot_id, shed_id, farm_id, initial_balance);
		storeData(lot_id, shed_id, farm_id, today, actual_bird_balance, food_csmpt, unit_csmpt); // Function for Insert in the TX_PRODUCTION_CONTROL table
	}
	
	stmt.close(); // Close the statement
	conn.commit(); // Commits the changes
	conn.close(); // Close connection
}

/* --- Method Call --- */
productionControlSimulator();