/* --- Global variables --- */
var CONST_ROOT_PACKAGE = "TEST_package_EMAESTRE"; // IR7 Package in Content section
var CONST_NEO_SCHEMA = "ELEAZARMAESTRE"; // IR7 User Schema
var TYPE_BIRD = ["BLANCO", "PIGMENTADO", "MIXTO"];

/* --- Functions --- */
function prepareUpdate(quantity_birds, farm_id, shed_id) {
	var status;
	var conn = $.db.getConnection(); // Returns a connection to the database
	var update_query = "CALL " + "\"" + CONST_NEO_SCHEMA + "\"" + "." +  "\"" + CONST_ROOT_PACKAGE + ".ComfortFarms.Stored_Procedures::UpdateShedStatus\" (?,?,?)";
	var update_stmt = conn.prepareStatement(update_query); // Prepare the SQL statement
	
	status = quantity_birds === 0 ? 'DESOCUPADO' : 'OCUPADO';

	// Set the statement with the required parameters	
	update_stmt.setInteger(1, farm_id); 
	update_stmt.setInteger(2, shed_id);
	update_stmt.setString(3, status);

	update_stmt.execute(); // Execute the statement
	update_stmt.close(); // Close the statement
	conn.commit(); // Commits the changes
	conn.close(); // Close connection
}

function storeData(lot_id, shed_id, farm_id, today, quantity_birds, type_bird) {
	var conn = $.db.getConnection(); // Returns a connection to the database
	var query = "CALL " + "\"" + CONST_NEO_SCHEMA + "\"" + "." +  "\"" + CONST_ROOT_PACKAGE + ".ComfortFarms.Stored_Procedures::HousingBirds\"(?,?,?,?,?,?);";
	var stmt = conn.prepareStatement(query); // Prepare the SQL statement

	// Set the statement with the required parameters	
	stmt.setString(1, lot_id);
	stmt.setInteger(2, shed_id);
	stmt.setInteger(3, farm_id);
	stmt.setDate(4, today); 
	stmt.setInteger(5, quantity_birds);
	stmt.setString(6, type_bird);

	stmt.execute(); // Execute the statement
	stmt.close(); // Close the statement
	conn.commit(); // Commits the changes
	conn.close(); // Close connection
}

function housingSimulator() {
	var lot_id,
		farm_id, 
		shed_id,  
		min = 0,  
		shed_capacity, 
		quantity_birds, 
		type_bird, 
		array_pos;
	
	var local = new Date();
	var offset = -240;
	var today = new Date(local.getTime() + (offset*60*1000));
    var year = today.getFullYear();
	
	var conn = $.db.getConnection(); // Returns a connection to the database
	var query = "SELECT FARM_ID, SHED_ID, CAPACITY FROM " + "\"" + CONST_NEO_SCHEMA + "\"" + ".\"OS_SHED\"";
	var stmt = conn.prepareStatement(query); // Prepare statement with the required query
	var rs = stmt.executeQuery(); // Execute the statement
	
	while(rs.next()) {
		// Get the results of the query
		farm_id = rs.getInteger(1);
		shed_id = rs.getInteger(2);
		shed_capacity = rs.getInteger(3);
		
		// Generate the data to insert
		lot_id = "L" + farm_id.toString() + "-" + year.toString().substr(-2);
		quantity_birds = (Math.floor(Math.random() * (shed_capacity - min + 1)) + min); // Random number for calculate the number of birds for the housing process
		prepareUpdate(quantity_birds, farm_id, shed_id); // Update the status of the shed
		array_pos = (Math.floor(Math.random() * ((TYPE_BIRD.length - 1) - min + 1)) + min); // Random number for choose a type bird 
		type_bird = TYPE_BIRD[array_pos]; 
		storeData(lot_id, shed_id, farm_id, today, quantity_birds, type_bird); // Function for Insert in the TX_SHED_CONTENT table		
	}
	
	stmt.close(); // Close the statement
	conn.commit(); // Commits the changes
	conn.close(); // Close connection
}

/* --- Method Call --- */
housingSimulator();