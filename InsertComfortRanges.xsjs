/* --- Global variables --- */
var CONST_ROOT_PACKAGE = "TEST_package_EMAESTRE"; // IR7 Package in Content section
var CONST_NEO_SCHEMA = "ELEAZARMAESTRE"; // IR7 User Schema

function storeData(farmID, climateFactorID, climateClassID, minRange, maxRange) {
	var conn = $.db.getConnection(); // Returns a connection to the database
	var query = "CALL " + "\"" + CONST_NEO_SCHEMA + "\"" + "." +  "\"" + CONST_ROOT_PACKAGE + ".ComfortFarms.Stored_Procedures::UpsertComfortRanges\"(?,?,?,?,?)";
	var stmt = conn.prepareStatement(query); // Prepare the SQL statement

	// Set the statement with the required parameters	
	stmt.setInteger(1, farmID); 
	stmt.setInteger(2, climateFactorID);
	stmt.setInteger(3, climateClassID);
	stmt.setInteger(4, minRange);
	stmt.setInteger(5, maxRange);
	
	stmt.execute(); // Execute the statement
	stmt.close(); // Close the statement
	conn.commit(); // Commits the changes
	conn.close(); // Close connection	
}

function processRequest() {
	try {
		var index;
		var data = $.request.body.asString();  
		var dataObject = JSON.parse(data);
		var farmID = dataObject.farmID;
		
		for(index = 0; index < dataObject.classRanges.length; index++) {
			storeData(
				farmID, 
				dataObject.classRanges[index].climateFactorID,
				dataObject.classRanges[index].climateClassID,
				dataObject.classRanges[index].minRange,
				dataObject.classRanges[index].maxRange
			);
		}
		$.response.setBody(JSON.stringify('Service successfully executed'));
		$.response.status = $.net.http.OK;
	}
	catch(e) {
		$.response.setBody(e.name + ": " +  e.message); // Set body with the selected string
		$.response.status = $.net.http.INTERNAL_SERVER_ERROR;
	}	
}

/* --- Method Call --- */
processRequest();