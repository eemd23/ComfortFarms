/* --- Global variables --- */
var CONST_ROOT_PACKAGE = "TEST_package_EMAESTRE"; // IR7 Package in Content section
var CONST_NEO_SCHEMA = "ELEAZARMAESTRE"; // IR7 User Schema
var API_KEY = "31038169867c4ec004c16c57166fc259"; // OpenWeather secret API Key

/* --- Functions --- */
function checkTime(minute) {
	if (minute < 10) {
		minute = "0" + minute;
	}
	
	return minute;
}

function orderedDate(day) {
	var date = day.getFullYear() + "-" + checkTime((day.getMonth() + 1)) + "-" + checkTime(day.getDate());
	return date;
}

function checkForecast(forecast_data, today) {
	var index,
		test_string,
		day_1 = new Date(today),
		day_2 = new Date(today),
		day_3 = new Date(today),
		object = {
		"firstDay": {},
		"secondDay": {},
		"thirdDay": {}	
	};
	
	day_1.setDate(today.getDate() + 1);
	day_2.setDate(today.getDate() + 2);
	day_3.setDate(today.getDate() + 3);
	
	for(index = 0; index < forecast_data.list.length; index++) {
		test_string = forecast_data.list[index].dt_txt.split(" ");
		if(test_string[0] === orderedDate(day_1)) {
			object.firstDay.temp = forecast_data.list[index].main.temp;
			object.firstDay.hum = forecast_data.list[index].main.humidity;
		}
		
		if(test_string[0] === orderedDate(day_2)) {
			object.secondDay.temp = forecast_data.list[index].main.temp;
			object.secondDay.hum = forecast_data.list[index].main.humidity;
		}
		
		if(test_string[0] === orderedDate(day_3)) {
			object.thirdDay.temp = forecast_data.list[index].main.temp;
			object.thirdDay.hum = forecast_data.list[index].main.humidity;
		}
	}
	
	return object;
}

function currentDate(sp) {
	var local = new Date();
	var offset = -240;
	var today = new Date(local.getTime() + (offset*60*1000));
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //As January is 0.
	var yyyy = today.getFullYear();

	dd = checkTime(dd);
	mm = checkTime(mm);
	
	return (yyyy + sp + mm + sp + dd);
}

function storeData(date, farm_id, temp, humidity, firstDay_temp, firstDay_hum, secondDay_temp, secondDay_hum, thirdDay_temp, thirdDay_hum, current_time) {
	var conn = $.db.getConnection(); // Returns a connection to the database
	var query = "CALL " + "\"" + CONST_NEO_SCHEMA + "\"" + "." +  "\"" + CONST_ROOT_PACKAGE + ".ComfortFarms.Stored_Procedures::UpdateComfortTable\"(?,?,?,?,?,?,?,?,?,?,?)";
	var stmt = conn.prepareStatement(query); // Prepare the SQL statement

	// Set the statement with the required parameters	
	stmt.setDate(1, date); 
	stmt.setInteger(2, farm_id);
	stmt.setFloat(3, temp);
	stmt.setFloat(4, humidity);
	stmt.setFloat(5, firstDay_temp);
	stmt.setFloat(6, firstDay_hum);
	stmt.setFloat(7, secondDay_temp);
	stmt.setFloat(8, secondDay_hum);
	stmt.setFloat(9, thirdDay_temp);
	stmt.setFloat(10, thirdDay_hum);
	stmt.setTime(11, current_time);
	
	stmt.execute(); // Execute the statement
	stmt.close(); // Close the statement
	conn.commit(); // Commits the changes
	conn.close(); // Close connection	
}

function getCurrentWeather() {
	var req, req_forecast, response, response_forecast, weather_data, forecast, forecast_data, latitude, longitude, id;
	var local = new Date();
	var offset = -240;
	var today = new Date(local.getTime() + (offset*60*1000)); // GMT -4 Time Zone
	var hour = new Date();
	hour.setHours(today.getHours()); 
	var minute = today.getMinutes();
	minute = checkTime(minute);
	var current_time = hour.getHours() + ":" + minute;
	var day;
	
	// Variables for perform the web request to the API 
	var destination_package = "ComfortFarms.Configurations"; // Name of the project. 
	var destination_name = "WeatherHTTP"; // Name of the .xshttpdest configuration file
	var dest_package_path = CONST_ROOT_PACKAGE + "." + destination_package; // Path to find the project
	var dest = $.net.http.readDestination(dest_package_path, destination_name); // Namespace for read the .xshttpdest file (package, objectName)
	var client;
	var client_forecast;
	var conn = $.db.getConnection(); // Returns a connection to the database
	var query = "SELECT FARM_ID, LATITUDE, LONGITUDE FROM " + CONST_NEO_SCHEMA + ".OS_FARM";
	var stmt = conn.prepareStatement(query); // Prepare statement with the required query
	var rs = stmt.executeQuery(); // Execute the statement
	

	while(rs.next())
	{
		id = rs.getInteger(1); // Farm SAP identity code
		latitude = rs.getFloat(2); // Latitude of the farm
		longitude = rs.getFloat(3); // Longitude of the farm
		
		// Call of the API to get the current weather
		req = new $.web.WebRequest($.net.http.GET, "/weather?" + "lat=" + latitude + "&lon=" + longitude + "&units=metric&appid=" + API_KEY); // Create a new http request, Get take the URL from the 
		client = new $.net.http.Client(); // Class "Client" for outbound connectivity
		client.request(req, dest); // Send a new request object to the given destination
		response = client.getResponse(); // Retrieve the response from the previously sent request synchronously/blocking 
		weather_data = JSON.parse(response.body.asString()); // Receives the response as a string and Parse the string as a JSON
		if(weather_data.main === undefined) {
			$.trace.debug("OpenWeather API connection error - Current Weather call.");
		}
		else {
			// Call of the API to get the forecast weather for the next 3 days
			req_forecast = new $.web.WebRequest($.net.http.GET, "/forecast?" + "lat=" + latitude + "&lon=" + longitude + "&units=metric&appid=" + API_KEY); // Create a new http request, Get take the URL from the 
			client_forecast = new $.net.http.Client();
			client_forecast.request(req_forecast, dest); // Send a new request object to the given destination
			response_forecast = client_forecast.getResponse(); // Retrieve the response from the previously sent request synchronously/blocking 
			forecast = JSON.parse(response_forecast.body.asString()); // Receives the response as a string and Parse the string as a JSON
			if(forecast.list === undefined) {
				$.trace.debug("OpenWeather API connection error - Forecast call.");
			}
			else {	
				day = new Date(currentDate('/')); // Get the current day in the following format -> yyyy/mm/dd
				forecast_data = checkForecast(forecast, day); // Return an object with temperature, humidity and comfort of the following 3 days
				
				storeData(
					today,
					id,
					parseFloat(weather_data.main.temp.toFixed(2)),
					weather_data.main.humidity,
					parseFloat(forecast_data.firstDay.temp.toFixed(2)),
					forecast_data.firstDay.hum,
					parseFloat(forecast_data.secondDay.temp.toFixed(2)),
					forecast_data.secondDay.hum,
					parseFloat(forecast_data.thirdDay.temp.toFixed(2)),
					forecast_data.thirdDay.hum,
					current_time
				); // Function for Insert/Update in the COMFORT_FARM table	
				
				client.close(); // Close the client for the current weather
				client_forecast.close(); // Close the client for the forecast
			}
		}
	}
	
	stmt.close(); // Close the statement
	conn.commit(); // Commits the changes
	conn.close(); // Close connection	
}
 
function processRequest() {
	getCurrentWeather(); // Call of the method
}

/* --- Method Call --- */
processRequest();