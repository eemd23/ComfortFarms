/* --- Global variables --- */
var CONST_ROOT_PACKAGE = "TEST_package_EMAESTRE"; // IR7 Package in Content section
var CONST_NEO_SCHEMA = "ELEAZARMAESTRE"; // IR7 User Schema

/* --- Functions --- */

//Waiting for the correct formule and early alert model
function comfortLevel(temperature, humidity) {
	var th_index, dewpoint, wet_bult_temp;
	
	dewpoint = temperature - ((100 - humidity) / 5);
	wet_bult_temp = temperature - ((temperature - dewpoint) / 3);
	th_index = ((0.4 * (temperature + wet_bult_temp)) + 4.8) / 100;
	
	return th_index;
}

// Simple function for check if the minute value of getMinute() is < 10 for add a 0 to the string.
function checkTime(minute) {
	if (minute < 10) {
		minute = "0" + minute;
	}
	
	return minute;
}

// Function for evaluate if a farm already exists in the farms array of the object weather_data
function existValue(arr, value) {
	var i, res = {
				"exist": false,
				"pos": null
			};
	
	for(i = 0; i < arr.length; i++) {
		if(arr[i].farmID === value) {
			res.exist = true;
			res.pos = i;
		}
	}
	
	return res;
}

// Function for organize the current date in the format yyyy-mm-dd
function currentDate(sp) {
	var local = new Date();
	var offset = -240;
	var today = new Date(local.getTime() + (offset*60*1000));
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //As January is 0.
	var yyyy = today.getFullYear();
	
	return (yyyy + sp + checkTime(mm) + sp + checkTime(dd));
}

// Check the missing factors on the array 
function missingFactor(res_object) {
	var alert, temp_factor = 1, hum_factor = 2;
	
	if(res_object.length === 1) {
		if(res_object.indexOf(temp_factor) > -1){
			alert = "La humedad y la densidad estan fuera de rango";
		}
		else if(res_object.indexOf(hum_factor) > -1){
			alert = "La temperatura y la densidad estan fuera de rango";
		}
		else {
			alert = "La temperatura y la humedad estan fuera de rango";
		}
	}
	else {
		if(res_object.indexOf(temp_factor) === -1){
			alert = "La temperatura está fuera de rango";
		}
		else if(res_object.indexOf(hum_factor) === -1){
			alert = "La humedad está fuera de rango";
		}
		else {
			alert = "La densidad está fuera de rango";
		}
	}
	
	return alert;
}

// Early Alerts generation
function getAlert(temp_classf, hum_classf, dens_classf, bird_age) {
	var alert_name;
	
	if(temp_classf === 1) {
		alert_name = "Emergencia";
	}
	else {
		var conn = $.db.getConnection(); // Returns a connection to the database
		var query = "CALL " + "\"" + CONST_NEO_SCHEMA + "\"" + "." +  "\"" + CONST_ROOT_PACKAGE + ".ComfortFarms.Stored_Procedures::GetAlert\"(?,?,?,?)";
		var stmt = conn.prepareCall(query); // Prepare the SQL statement
		
		if(bird_age >= 0 && bird_age <= 14) {
			dens_classf = 10; // Because, when the lot is between this ranges, the density is always low
		}
		
		stmt.setInteger(1, temp_classf);
		stmt.setInteger(2, hum_classf);
		stmt.setInteger(3, dens_classf);
		stmt.setInteger(4, bird_age);
		
		stmt.execute(); // Execute the statement
		
		var rs = stmt.getResultSet();
		
		if(rs.next()) {
			alert_name = rs.getString(1);
		} 
		else {
			alert_name = "No alert";
		}
		
		stmt.close(); // Close the statement
		conn.commit(); // Commits the changes
		conn.close(); // Close connection	
	}
	
	return alert_name;
}

function earlyAlertGeneration(temperature, humidity, shed_density, farm_id, bird_age) {
	var conn = $.db.getConnection(); // Returns a connection to the database
	var query = "CALL " + "\"" + CONST_NEO_SCHEMA + "\"" + "." +  "\"" + CONST_ROOT_PACKAGE + ".ComfortFarms.Stored_Procedures::ClimateClassf\"(?,?,?,?,?,?,?)";
	var stmt = conn.prepareCall(query); // Prepare the SQL statement
	var temp_factor = 1, hum_factor = 2, dens_factor = 3;
	var res_object = [];
	var alert;
	
	// Set the statement with the required parameters
	stmt.setInteger(1, temperature);
	stmt.setInteger(2, humidity);
	stmt.setInteger(3, shed_density);
	stmt.setInteger(4, temp_factor);
	stmt.setInteger(5, hum_factor);
	stmt.setInteger(6, dens_factor);
	stmt.setInteger(7, farm_id);
	
	stmt.execute(); // Execute the statement
	
	var rs = stmt.getResultSet();
	
	while(rs.next()) {
		res_object.push({
			climateClassfID: rs.getInteger(1),
			climateFactorID: rs.getInteger(2)
		});
	}
	
	if(res_object.length === 3) {
		alert = getAlert(res_object[0].climateClassfID, res_object[1].climateClassfID, res_object[2].climateClassfID, bird_age);
	}
	else {
		if(res_object.length === 0) {
			alert = "Todos los factores estan fuera de rango.";
		}
		else {
			var index, array = [];
			for(index = 0; index < res_object.length; index++) {
				array.push(res_object[index].climateFactorID);
			}
			alert = missingFactor(array);
		}	 
	}
	
	stmt.close(); // Close the statement
	conn.commit(); // Commits the changes
	conn.close(); // Close connection
	
	return alert;
}

// Function to calculate the previous density of the shed
function previousDensity(farm_id, shed_id, previous_date, shed_length, shed_width, shed_density) {
	var previous_density = shed_density, bird_balance;
	var conn = $.db.getConnection(); // Returns a connection to the database
	var query = "CALL " + "\"" + CONST_NEO_SCHEMA + "\"" + "." +  "\"" + CONST_ROOT_PACKAGE + ".ComfortFarms.Stored_Procedures::GetPreviousBirdBalance\"(?,?,?)";
	var stmt = conn.prepareStatement(query); // Prepare the SQL statement

	// Set the statement with the required parameters	
	stmt.setInteger(1, farm_id);
	stmt.setInteger(2, shed_id);
	stmt.setDate(3, previous_date);
	stmt.execute(); // Execute the statement
	
	var rs = stmt.getResultSet();
	
	if(rs.next()) {
		bird_balance = rs.getInteger(1);
		previous_density = Math.round(bird_balance / (shed_length * shed_width));
	}
	
	stmt.close(); // Close the statement
	conn.commit(); // Commits the changes
	conn.close(); // Close connection
	
	return previous_density;
}

// Function to perform the linear extrapolation process of the density
function densityExtrapolation(shed_density, today, previous_density, previous_date) {
	var res;
	var extrapolationValue = new Date(today);
	
	extrapolationValue.setDate(today.getDate() + 1);
	
	res = previous_density + (shed_density - previous_density) * ((extrapolationValue.getTime() - previous_date.getTime()) / (today.getTime() - previous_date.getTime()));
	
	return res;
}

/* --- Main Functions --- */

// Function that make a request to the database and create an object in json format for the response
function getWeatherInfo() {
	var farm_id,
		shed_id,
		result, 
		lastUpdate, 
		shed_density, 
		quantity_birds, 
		temperature,
        humidity,
    	temperatureDay1,
    	humidityDay1,
    	temperatureDay2,
    	humidityDay2,
    	temperatureDay3,
    	humidityDay3,
    	bird_age,
    	previous_density;
	
	// Variables for dates operations
	var local = new Date();
	var offset = -240;
	var today = new Date(local.getTime() + (offset*60*1000)); // GMT -4 Time Zone
	var hour = new Date();
	hour.setHours(today.getHours()); 
	var minute = today.getMinutes();
	minute = checkTime(minute);
	var current_time = hour.getHours() + ":" + minute;
	var current_date = currentDate('-');
	var oneDay = 24*60*60*1000;
	var previous_date = new Date(today);
	previous_date.setDate(today.getDate() - 1);
	
	var weather_data = {
			"currentDate": current_date,
			"currentTime": current_time,
			"farms": []
		};
	
	// Variables for perform the DB connection and the stored procedure call
	var conn = $.db.getConnection(); // Returns a connection to the database
	var query = "CALL " + "\"" + CONST_NEO_SCHEMA + "\"" + "." +  "\"" + CONST_ROOT_PACKAGE + ".ComfortFarms.Stored_Procedures::ClientQueryComfortTable\"(?)";
	var stmt = conn.prepareCall(query); // Prepare the SQL statement
	 
	stmt.setDate(1, current_date); // Set the statement with the required parameters
	stmt.execute(); // Execute the statement
	
	var rs = stmt.getResultSet();
	
	// rs is the result set of the query, rs.next() is a function that point row by row of the table
	while(rs.next())
	{
		farm_id = rs.getInteger(1);
		shed_id = rs.getInteger(20);
		result = existValue(weather_data.farms, farm_id);
		lastUpdate = rs.getTime(12);
		lastUpdate = lastUpdate.getHours() + ":" + checkTime(lastUpdate.getMinutes()); 
		quantity_birds = rs.getInteger(16);
		shed_density = Math.round(quantity_birds / (rs.getInteger(14) * rs.getInteger(15)));
		previous_density = previousDensity(farm_id, shed_id, previous_date, rs.getInteger(14), rs.getInteger(15), shed_density);
		
		// Get the temperature and humidity values
		temperature = Math.round(rs.getFloat(4));
        humidity = rs.getFloat(5);
    	temperatureDay1 = Math.round(rs.getFloat(6));
    	humidityDay1 = rs.getFloat(7);
    	temperatureDay2 = Math.round(rs.getFloat(8));
    	humidityDay2 = rs.getFloat(9);
    	temperatureDay3 = Math.round(rs.getFloat(10));
    	humidityDay3 = rs.getFloat(11);
    	
    	// Get the lot age
    	bird_age =  Math.floor(Math.abs(rs.getDate(19).getTime() - today.getTime())/oneDay);
    	
		if(result.exist) {
            weather_data.farms[result.pos].sheds.push({
            	shedName: rs.getString(13),
	        	quantityBirds: quantity_birds,
	        	environment: rs.getString(17),
	        	density: shed_density,
	        	comfortLevel: parseFloat(comfortLevel(rs.getFloat(4), rs.getFloat(5)).toFixed(2)),
	        	currentEarlyAlert: earlyAlertGeneration(temperature, humidity, shed_density, farm_id, bird_age),
	        	earlyAlertDay1: earlyAlertGeneration(temperatureDay1, humidityDay1, densityExtrapolation(shed_density, today, previous_density, previous_date), farm_id, bird_age + 1),
	        	earlyAlertDay2: earlyAlertGeneration(temperatureDay2, humidityDay2, densityExtrapolation(shed_density, today, previous_density, previous_date), farm_id, bird_age + 2),
	        	earlyAlertDay3: earlyAlertGeneration(temperatureDay3, humidityDay3, densityExtrapolation(shed_density, today, previous_density, previous_date), farm_id, bird_age + 3)
            });
        }
        else {
        	weather_data.farms.push({
            	farmID: farm_id,
            	farmName: rs.getString(2),
    	        qualification: rs.getInteger(3),
    	        currentTemperature: temperature,
    	        currentHumidity: humidity,
    	    	temperatureDay1: temperatureDay1,
    	    	humidityDay1: humidityDay1,
    	    	temperatureDay2: temperatureDay2,
    	    	humidityDay2: humidityDay2,
    	    	temperatureDay3: temperatureDay3,
    	    	humidityDay3: humidityDay3,
    	    	lastUpdateTime: lastUpdate,
    	        sheds: [{
    	        	shedName: rs.getString(13),
    	        	quantityBirds: quantity_birds,
    	        	environment: rs.getString(17),
    	        	density: shed_density,
    	        	comfortLevel: parseFloat(comfortLevel(rs.getFloat(4), rs.getFloat(5)).toFixed(2)),
    	        	currentEarlyAlert: earlyAlertGeneration(temperature, humidity, shed_density, farm_id, bird_age),
    	        	earlyAlertDay1: earlyAlertGeneration(temperatureDay1, humidityDay1, densityExtrapolation(shed_density, today, previous_density, previous_date), farm_id, bird_age + 1),
    	        	earlyAlertDay2: earlyAlertGeneration(temperatureDay2, humidityDay2, densityExtrapolation(shed_density, today, previous_density, previous_date), farm_id, bird_age + 2),
    	        	earlyAlertDay3: earlyAlertGeneration(temperatureDay3, humidityDay3, densityExtrapolation(shed_density, today, previous_density, previous_date), farm_id, bird_age + 3)	        
    	        }]
            });
        }
	}
	
	stmt.close(); // Close the statement
	conn.commit(); // Commits the changes
	conn.close(); // Close connection	
	
	return weather_data;
}

// Function for process the client request
function processRequest() {
	try {
		var api_call;
	
		api_call = getWeatherInfo(); // Call of the method 
		
		$.response.contentType = "application/json"; // Content type of the response
		$.response.setBody(JSON.stringify(api_call)); // Set body with the selected string
		$.response.status = $.net.http.OK; // status = OK when all the task are done
	}
	catch(e) {
		$.response.contentType = "text/plain"; // Content type of the response
		$.response.setBody(e.name + ": " +  e.message); // Set body with the selected string
	}
}

/* --- Method Call --- */
processRequest();