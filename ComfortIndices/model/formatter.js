sap.ui.define([], function() {
  "use strict";

  return {
	  lessThan(minRange, stateMaxInput, nextMinInput, nextMaxInput) {
		  let maxInput, state; 
		  maxInput = stateMaxInput === "" ? stateMaxInput : parseInt(stateMaxInput, 10);
		  minRange = minRange === "" ? minRange : parseInt(minRange, 10);
		  
		  if(minRange === "") {
			  state = 'None';  
		  }
		  else if(maxInput === "") {
			  state = 'Error';
		  }
		  else if(minRange < maxInput) {
			  state = 'Success';
		  }
		  else {
			  state = 'Error';
		  }
		  
		  if(state === 'Success') {
			  nextMaxInput.enabled = true;
			  nextMinInput.value = maxInput + 1;
      	  }
		  else {
			  nextMaxInput.enabled = false;
			  nextMaxInput.value = "";
			  nextMinInput.value = "";
           }
		  
		  return state;
	  },
	  
	  greaterThan(maxRange, stateMinInput, nextMinInput, nextMaxInput) {
		  let minInput, state; 

		  minInput = stateMinInput === "" ? stateMinInput : parseInt(stateMinInput, 10);
		  maxRange = maxRange === "" ? maxRange : parseInt(maxRange, 10);
		        	
		  if(maxRange === "") {
			  state = 'None';  
		  }
		  else if(minInput === "") {
			  state = 'Error';
		  }
		  else if(maxRange > minInput) {
			  state = 'Success';
		  }
		  else {
			  state = 'Error';
		  }
		  
		  if(state === 'Success') {
			  nextMaxInput.enabled = true;
			  nextMinInput.value = maxRange + 1;
      	  }
		  else {
			  nextMaxInput.enabled = false;
			  nextMaxInput.value = "";
			  nextMinInput.value = "";
           }
		  
		  return state;
	  },
	  
	  enableSaveButton(tempInputValue, humInputValue, densInputValue) {
		  let tempInput, humInput, densInput;
		  
		  tempInput = this.getView().byId("extremeMaxRange").getValueState();
		  humInput =  this.getView().byId("wetMaxRange").getValueState(); 
		  densInput =  this.getView().byId("highMaxRange").getValueState();
		  
		  if(tempInput === 'Success' && humInput === 'Success' && densInput === 'Success') {
			  return true;
		  }
		  else {
			  return false;
		  }
	  }
  };
});
