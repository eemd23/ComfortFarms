sap.ui.define([
    "ComfortIndices/controller/BaseController",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator" 
], function(BaseController, Filter, FilterOperator) {
	"use strict";
	return BaseController.extend("ComfortIndices.controller.Main", {

		onInit: function() {
			this.getRouter().getRoute("main").attachPatternMatched(this._onRouteMatched, this);
        },

		_onRouteMatched: function(oEvent) {
			let clean, data;
			
			console.log('On main matched');
			this._oModel = this.getModel('data');
			this._oModel.setData({
                "parameters": {
                    "confirmEnable" : false
                }
            });
            this.getFarms(this._oModel, '/farm');
            clean = this.getModel('cleanModel');
        	data = clean.getData();
        	console.log("Clean data: ", data);
        	this.getModel('validation').setData(JSON.parse(JSON.stringify(data)));
		},

        handleValueFarm: function(oEvent){
            this.Farm = sap.ui.xmlfragment("ComfortIndices.view.fragments.FarmDialog", this);
            this.Farm.setModel(this._oModel, "data");
            this.Farm.open();
        },

        selectFarm: function(oEvent){
			let farm = oEvent.getParameters().selectedItem.getBindingContext('data').getObject();
            this._oModel.setProperty("/parameters/selectedFarm", farm);
            this._oModel.setProperty("/parameters/confirmEnable", true);
        },
		
         farmFilter: function(oEvent) {
            let sValue = oEvent.getParameter("value");
            let oFilter = new sap.ui.model.Filter("farmName", sap.ui.model.FilterOperator.Contains, sValue);
            let oBinding = oEvent.getSource().getBinding("items");
            oBinding.filter([oFilter]);
        },

        nextPage: function(oEvent) {
            this.getRouter().navTo('secondary');
        }
	});
});
