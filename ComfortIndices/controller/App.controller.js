sap.ui.define([
  "ComfortIndices/controller/BaseController"
], function(BaseController) {
  "use strict";

  return BaseController.extend("ComfortIndices.controller.App", {
    onInit: function() {
      this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
    }
  });
});
