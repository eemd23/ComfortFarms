sap.ui.define([
  "sap/ui/core/mvc/Controller",
  "sap/m/MessageBox",
  "ComfortIndices/model/formatter",
  "sap/m/MessageToast"
  
], function(Controller, MessageBox, formatter, MessageToast) {
  "use strict";
    return Controller.extend("ComfortIndices.controller.BaseController", {
    	
     formatter: formatter,
      /**
        * Convenience method for accessing the router in every controller of the application.
        * @public
        * @returns {sap.ui.core.routing.Router} the router for this component
        */
      getRouter: function () {
        return this.getOwnerComponent().getRouter();
      },

      /**
         * Convenience method for getting the view model by name in every controller of the application.
         * @public
         * @param {string} sName the model name
         * @returns {sap.ui.model.Model} the model instance
         */
      getModel: function (sName) {
        return this.getView().getModel(sName);
      },

      /**
         * Convenience method for setting the view model in every controller of the application.
         * @public
         * @param {sap.ui.model.Model} oModel the model instance
         * @param {string} sName the model name
         * @returns {sap.ui.mvc.View} the view instance
         */
      setModel: function (oModel, sName) {
        return this.getView().setModel(oModel, sName);
      },

      /**
         * Convenience method for getting the resource bundle.
         * @public
         * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
         */
      getResourceBundle: function () {
        return this.getOwnerComponent().getModel("i18n").getResourceBundle();
      },

      setBackendData : function(oModel, path, srvModel, srvPath, filters = []) {
          srvModel.read(srvPath, {
          filters,
          success : res => {
            console.log(res);
            if (res.results[0] !== undefined) {
              console.log(res);
              oModel.setProperty(path, res.results);
            }
          },
          error : err => {
            oModel.setProperty(path, []);
            console.log(err);
            MessageBox.error(JSON.parse(err.responseText).error.message.value, { title : err.statusCode });
          }
        })
      },

      loadFragment : function(path) {
        let fragment = sap.ui.xmlfragment(path, this)
        this.getView().addDependent(fragment)
              
        return fragment;
      },
      
      onNavBack: function() {
          this.getRouter().navTo("main", {}, true );
      },
      
      getURL : function(endPoint){
    	  let configModel = this.getModel('configurationFile');
    	  
    	  return configModel.getProperty('/serviceURL') + endPoint;
      },
      
      getFarms : function(oModel, path) {
    	  $.ajax({
    		  url: this.getURL("GetFarms.xsjs"),
    		  type: 'GET',
    		  success: res => {
    			  console.log("GET result: ", res);
    			  if (res.farms[0] !== undefined) {
    				  oModel.setProperty(path, res.farms);
    			  }
    		  },
    		  error: err => {
    			  oModel.setProperty(path, []);
    			  console.log(err);
    			  MessageBox.error(JSON.parse(err.responseText).error.message.value, { title : err.statusCode });
    		  }
    	  });
      	},
      	
      	sendData: function(data) {
      		$.ajax({
      		  url: this.getURL("InsertComfortRanges.xsjs"),
      		  type: 'POST',
      		  data: JSON.stringify(data),
      		  success: res => {
      			 console.log("POST result: ", res);
      			 MessageBox.success("Successfully data save!", {
	      			    title: "Success",
	      			    actions: [sap.m.MessageBox.Action.OK],
	      			    onClose: oAction => {
	      			    	this.onNavBack();
	      			    }    
      			    });
      		  },
      		  error: err => {
      			  console.log(err);
      			  MessageBox.error(JSON.parse(err.responseText).error.message.value, { 
      				  title : err.statusCode 
      			  	});
      		  }
      	  });
      	}
    });
});
