sap.ui.define([
    'ComfortIndices/controller/BaseController'
], function (BaseController) {
    "use strict";
    return BaseController.extend("ComfortIndices.controller.Secondary", {

        onInit: function () {
            this.getRouter().getRoute("secondary").attachPatternMatched(this._onRouteMatched, this);
        },

        _onRouteMatched: function (oEvent) {
            console.log('On secondary matched');
			this._oModel = this.getModel('data');
			this.validationModel = this.getModel('validation');
        },
        
        validateIntInput: function (o) {
        	let input= o.getSource();
        	let length = 10;
        	let value = input.getValue();
        	let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

        	if (regex.test(value)) {
        		return true;
        	}
        	else {
	        	let aux = value.split('').filter(char => {
	        		if (/^[0-9]$/.test(char)) {
	        			if (char !== '.') {
	        				return true;
	        			}
	        		}
	        	}).join('');
	        	
	        	value = aux.substring(0, length);
	        	input.setValue(parseInt(value,10));
	        } 
        },
        
        fahrenheitToCelsius: function(temp) {
        	return ((temp - 32) / 1.8);
        },
        
        kelvinToCelsius: function(temp) {
        	return (temp - 273.15);
        },
        
        onPress: function(event) {
        	let farm_id,
        		selectedKey, 
        		coldMinRange, 
        		coldMaxRange, 
        		temperedMinRange, 
        		temperedMaxRange,
        		hotMinRange,
        		hotMaxRange,
        		extremeMinRange,
        		extremeMaxRange,
        		temperatureArray = [
        			"coldMinRange",
        			"coldMaxRange",
        			"temperedMinRange",
        			"temperedMaxRange",
        			"hotMinRange",
        			"hotMaxRange",
        			"extremeMinRange",
        			"extremeMaxRange"
        		];
        	
        	farm_id = this.getModel('data').getProperty("/parameters/selectedFarm/farmID");
        	this.validationModel.setProperty("/farmID", farm_id);
        	selectedKey = this.getView().byId("selectedTempScale").getSelectedKey();
        	const data = this.validationModel.getData();
    		let dataCpy = JSON.parse(JSON.stringify(data));
    		let resObject = {
    				farmID: dataCpy.farmID,
    				classRanges: []
    		}
        	
    		console.log("Data copy before Fahrenheit/Kelvin evaluation.");
    		console.log(dataCpy);
        	if(selectedKey === "Fahrenheit" || selectedKey === "Kelvin") {
        		temperatureArray.forEach(property => {
        			let value = dataCpy[property].value;
        			
        			if(selectedKey === "Fahrenheit") { 
        				dataCpy[property].value = this.fahrenheitToCelsius(value);
        			}
        			else {
        				dataCpy[property].value = this.kelvinToCelsius(value);
        			}
        		});
        	}
        	
        	console.log("Data copy after Fahrenheit/Kelvin evaluation.");
        	console.log(dataCpy);
        	
        	delete dataCpy.finalInput;
        	delete dataCpy.saveButton;
        	
        	console.log("Data copy after delete finalInput and saveButton");
        	console.log(dataCpy);
        	
        	for(const key in dataCpy) {
        		if(typeof dataCpy[key] === 'object') {
        			dataCpy[key].value = parseInt(Math.round(parseFloat(dataCpy[key].value)));
        		}
        	}
        	console.log("DataCpy After Parse");
        	console.log(dataCpy);
        	
        	for(const key in dataCpy) {
        		for(const evaluate in dataCpy) {
    				if(dataCpy[evaluate].climateClassID === dataCpy[key].climateClassID) {
    					if(dataCpy[evaluate].RangeID === 1 && dataCpy[key].RangeID === 2) {
    						resObject.classRanges.push({
        						climateFactorID: dataCpy[evaluate].climateFactorID,
        						climateClassID: dataCpy[evaluate].climateClassID,
        						minRange: dataCpy[evaluate].value,
        						maxRange: dataCpy[key].value
        					});
    					}
    				}
    			}
        	}
        	
        	console.log("resObject");
        	console.log(resObject);
        	
        	this.sendData(resObject);
        }
    });
});
